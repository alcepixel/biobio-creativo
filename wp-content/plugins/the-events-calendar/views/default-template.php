<?php
/**
 * Default Events Template
 * This file is the basic wrapper template for all the views if 'Default Events Template'
 * is selected in Events -> Settings -> Template -> Events Template.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/default-template.php
 *
 * @package TribeEventsCalendar
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

get_header(); ?>
<div class="javo_post_header_fancy" style="
			position:relative;
			background-color:#FFFFFF;
			background-image:url('http://biobiocreativo.com/wp-content/uploads/2015/06/biobio.png');
			background-repeat:no-repeat;
			background-position:left top;
			height:150px;">
			<div class="container" style="display:table;">
				<div style="
			left:0px;
			width:100%;
			height:150px;
			display:table-cell;
			vertical-align:middle;
			text-align:left;">
					<h1 style="
			color:#ffffff;
			font-size:24pt;">EVENTOS</h1>
					<h4 style="
			color:#000000;
			font-size:12pt;"></h4>
				</div>
			</div>
		</div>
	<div id="tribe-events-pg-template">
		<?php tribe_events_before_html(); ?>
		<?php tribe_get_view(); ?>
		<?php tribe_events_after_html(); ?>
	</div> <!-- #tribe-events-pg-template -->
<div class="container formulario-eventos">
	<div class="vc_row wpb_row vc_row-fluid">
		<div class="vc_col-sm-12 wpb_column vc_column_container ">
			<div class="javo-fancy-title-section">
				<h2 style="color:#ed7900;">PUBLICA TU EVENTO</h2>
				<div class="hr-wrap">
					<span class="hr-inner" style="border-color:#ed7900;">
						<span class="hr-inner-style"></span>
					</span>
				</div> <!-- hr-wrap -->
				<div class="javo-fancy-title-description text-center" style="position:relative;">
					<div style="color:#646464;">
					Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras nunc velit
					</div>
				</div>
				
					<form class="navbar-form navbar-left" role="search" >
						<div class="form-group">
					    	<input type="text" class="form-control" placeholder="Titulo" >
						</div>
						<div class="form-group" >
					        <h3 class="form-title">DETALLE:</h3>
					        <div class="row">
					        	<div class="col-sm-4">
					        		<label>Fecha:</label>
					          	<div class='input-group date' id='datetimepicker1'>
					                <input type='text' class="form-control detalle-input" />
					                <span class="input-group-addon">
					                    <span class="glyphicon glyphicon-calendar"></span>
					                </span>
					            </div>
					        	</div>
					        	<div class="col-sm-4">
					        		<label>Precio:</label>
					            <input type="text" class="form-control detalle-input">
					        	</div>
					        	<div class="col-sm-4">
					        		<label>Categoria evento:</label>
					            <input type="text" class="form-control detalle-input">
					        	</div>
					        </div>
						</div>
						<div class="form-group" >
					        <h3 class="form-title">ORGANIZADOR:</h3>
							<div class="row">
							    <div class='col-sm-6'>
							    	<div class="row">
							    	    <div class='col-sm-12'>
								    		<div class="col-sm-3">
								    			<label>Nombre:</label>
								    		</div>
								    		<div class="col-sm-9">
								    			<input type="text" class="form-control">
								    		</div>
								    	</div>
								    </div>
								    <div class="row">
							    	    <div class='col-sm-12'>
								    		<div class="col-sm-3">
								    			<label>Email:</label>
								    		</div>
								    		<div class="col-sm-9">
								    			<input type="text" class="form-control" >
								    		</div>
								    	</div>
								    </div>
							    </div>
							    <div class='col-sm-6'>
							    	<div class="row">
							    	    <div class="row">
							    	    <div class='col-sm-12'>
								    		<div class="col-sm-3">
								    			<label>Telefono:</label>
								    		</div>
								    		<div class="col-sm-9">
								    			<input type="text" class="form-control">
								    		</div>
								    	</div>
								    </div>
								    <div class="row">
							    	    <div class='col-sm-12'>
								    		<div class="col-sm-3">
								    			<label>Web:</label>
								    		</div>
								    		<div class="col-sm-9">
								    			<input type="text" class="form-control">
								    		</div>
								    	</div>
								    </div>
								    </div>
							    </div>
							</div>
						</div>
							<div class="form-group">
						        <h3 class="form-title">UBICACIÓN:</h3>
								<div class="row">
								    <div class='col-sm-6'>
								    	<div class="row">
								    	    <div class='col-sm-3'>
									    	<label>Dirección:</label><br>
									    	</div>
								    	    <div class='col-sm-9'>
									    	<input type="text" class="form-control" >
									    	</div>
									    </div>
								    </div>
								    <div class='col-sm-6'>
								    	<div class="row">
								    	    <div class='col-sm-3'>
									    	<label>Código Postal:</label><br>
									    	</div>
								    	    <div class='col-sm-9'>
									    	<input type="text" class="form-control" >
									    	</div>
									    </div>
								    </div>
								</div>
								<div class="row">
								   	<div class='col-sm-12'>
									   	<label>Comentario:</label><br>
									   	<textarea></textarea>
									   	<input id="input-1" type="file" class="file">
									   	<button>ENVIAR</button>
									</div>
								</div>
								</div>
							</div>
					</form>
			</div>
		</div>
	</div>
</div>

	<script type="text/javascript">
	    $(function () {
	        $('#datetimepicker1').datetimepicker();
	    });
	</script>
<?php get_footer(); ?>