<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'biobiocreativo');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'k0<#B%<nB*,(1Y;|&[pY.A+pq~x%*b8j?fwc%k[=sePN%q:2M/v`Ivc{>bc%2N8V');
define('SECURE_AUTH_KEY',  '?u8urQ.rbx==>MDudh*yZq!uJIl*f?&-V(}>?Dm<0O+dO:&8(o=j?;5o<>x+X*E3');
define('LOGGED_IN_KEY',    '5V2(BS.:(Z_#./Z}njNSYO&$,Aw/unJi&&2}8$(aO{TT6!|Btv9/sc||$y8nGE<S');
define('NONCE_KEY',        'GK&WY?C3X(HhX4]|bYW%m$kSc$},[PHlDk6+.}*}}eGd}q!b0Zae(E2v&cs-IGpJ');
define('AUTH_SALT',        'u_Hn|Jp7s-_7<*]tKBDyxIKEw <:NR#0r?]DZgls)Y~Un|lMzmUbNn|ky}8~t(QO');
define('SECURE_AUTH_SALT', '`j&7c5,bD+^|1/SnpW6(>L,4NjM~G:#=9sQ,tIyP[=A!/p5JcjZYSQl1fGU?)gue');
define('LOGGED_IN_SALT',   '^^.c%nPeh5z5yiD#?%{vu.uYfa[I?+7s5IA1@+zi*Jq^,nyyoS}l`|XtiVp]fWF*');
define('NONCE_SALT',       'x R5QjEm@l-+|0gBx-7l>i<:W3^A=l1@GrFy7,6`(b 9QknbLDE/M*P@A~CDo|</');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
